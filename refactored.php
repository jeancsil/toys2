<?php

/**
 * Caso fosse um projeto um pouco maior eu utilizaria a biblioteca HttpFoundation.
 * Validaria as entradas dos usuários.
 * Retiraria o elseif e exit desnecessários.
 */
use Symfony\Component\HttpFoundation\RedirectResponse;

$sessionLoggedIn = filter_var($this->session->get('loggedin'), FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
$cookieLoggedIn = filter_var($this->cookies->get('loggedin'), FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);

if ($sessionLoggedIn === true || $cookieLoggedIn === true) {
    return new RedirectResponse('http://www.google.com');
}
